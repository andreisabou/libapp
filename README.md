## Available Scripts

Step one run in the project directory:

Install dependencies:
## `yarn install`

In the project directory, you can run:
### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`
Launches the test runner in the interactive watch mode.\


### After the backend application starts, a user will be created automatically with following LOGIN-CREDENTIALS: 
### ~user: user, pass:test~

###We have Backup data in case of backend application is not working
###Just for purpose demonstration


Additional information: currently, I use: -node: v14.16.1 | -yarn: 1.22.10 | -npm: 6.14.12
