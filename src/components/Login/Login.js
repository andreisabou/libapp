import React, { useState, useContext } from 'react';
import { TextField, Button } from '@material-ui/core';

import { AuthContext } from "../../shared/Auth";
import { login } from "../../repositories/store/actions";
import { makeStyles } from "@material-ui/core/styles";
import { apiGetUser } from "../../repositories/store/api";

const useStyles = makeStyles((theme) => ({
    container: {
        borderRadius: "10px",
        margin: "0 auto",
        maxWidth: "800px",
        border: "1px solid rgba(0, 0, 0, 0.12)"
    },
    containerHeader: {
        display: "flex",
        alignItems: "center",
        height: "50px",
        borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        marginBottom: "20px",
        backgroundColor: "#e8e8e8",
        padding: "10px",
        borderTopLeftRadius: "9px",
        borderTopRightRadius: "9px"
    },
    containerBody: {
        margin: "10px"
    },
    spacing: {
        marginBottom: "20px"
    },
    button: {
        marginTop: "45px",
        float: "right"
    },
    errorMessage: {
        color: "red"
    }
}));

const Login = () => {
    const classes = useStyles();
    const [loginValues, setLoginValues] = useState({ email: '', password: '' });
    const [isError, setError] = useState(false);

    const { setCurrentUser } = useContext(AuthContext);

    const handleFieldChange = e => {
        const { name, value } = e.target;
        setLoginValues({ ...loginValues, [name]: value });
    };

    const handleLogin = async () => {
        try {
            const response = await login(loginValues);
            setCurrentUser(response.data);
        } catch (e) {
            //back-up in case the backend application is not working
            //otherwise, ignore this
            apiGetUser(loginValues).then(response => {
                setCurrentUser(response);
                localStorage.setItem(
                    'token',
                    response.token
                );
            }).catch(err => {
                setError(true);
                console.log('LOGIN In - Error: ', err);
                console.log('For login please use the following credentials: |email:user, password:test|');
            });
        }
    };

    return (
        <div className={classes.container}>
            <div className={classes.containerHeader}>
                Enter your user name and password below.
            </div>
            <form className={classes.containerBody}>
                <TextField
                    id="filled-password-input"
                    label="Email"
                    value={loginValues.email}
                    type="email"
                    name="email"
                    variant="outlined"
                    fullWidth
                    className={classes.spacing}
                    onChange={handleFieldChange}
                />

                <TextField
                    id="filled-password-input"
                    label="Password"
                    value={loginValues.password}
                    type="password"
                    name="password"
                    variant="outlined"
                    fullWidth
                    className={classes.spacing}
                    onChange={handleFieldChange}
                />
                {isError && <p className={classes.errorMessage}>Sorry, something went wrong. Please check the console.</p>}
            </form>
            <Button className={classes.button} variant="contained"
                color="primary"
                disabled={!loginValues.password && !loginValues.email}
                onClick={handleLogin}>
                Login
            </Button>
        </div>
    );
};

export default Login;
