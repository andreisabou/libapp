import React, { useState, useContext } from 'react';
import { AppBar, Toolbar, IconButton, Menu, MenuItem, Badge } from "@material-ui/core";
import FavoriteIcon from '@material-ui/icons/Favorite';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { AuthContext } from "../../shared/Auth";
import PropTypes from 'prop-types';
import { useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import { selectors } from "../../repositories/store/reducer";
import HomeIcon from '@material-ui/icons/Home';

const Header = () => {
    const history = useHistory();
    const [anchorEl, setAnchorEl] = useState(null);
    const { setCurrentUser } = useContext(AuthContext);
    const cartItems = useSelector(state => selectors.getCartItems(state));
    const favouriteItems = useSelector(state => selectors.getFavouriteItems(state));

    const menuId = 'primary-search-account-menu';
    const quantity = cartItems.reduce((accumulator, currentVal) => accumulator + currentVal.qty, 0);

    const handleMenuClose = () => {
        history.push("/books");
        setAnchorEl(null);
    };

    const handleProfileMenuOpen = (e) => {
        setAnchorEl(e.target);
    };
    const handleLogOut = () => {
        setCurrentUser(null);
    };
    const isMenuOpen = Boolean(anchorEl);

    const goToShippingCart = () => {
        history.push("/cart");
    };

    const goToMainMenu = () => {
        history.push("/books");
    };

    const goToFavourite = () => {
        history.push("/favourite");
    };

    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={menuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem onClick={handleMenuClose}>Home</MenuItem>
            <MenuItem onClick={handleLogOut}>Log Out</MenuItem>
        </Menu>
    );

    return (
        <>
            <AppBar position="static">
                <Toolbar style={{ justifyContent: "space-between" }}>
                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        onClick={goToMainMenu}
                    >
                        <HomeIcon fontSize="large" />
                    </IconButton>

                    <div>
                        <IconButton
                            color={"inherit"}
                            onClick={goToFavourite}>
                            <Badge color="secondary"
                                badgeContent={favouriteItems.length}>
                                <FavoriteIcon />
                            </Badge>
                        </IconButton>
                        <IconButton
                            color={"inherit"}
                            onClick={goToShippingCart}>
                            <Badge badgeContent={quantity} color="secondary">
                                <ShoppingCartIcon />
                            </Badge>
                        </IconButton>
                        <IconButton
                            edge="end"
                            aria-controls={menuId}
                            aria-label="account of current user"
                            aria-haspopup="true"
                            onClick={handleProfileMenuOpen}
                            color="inherit"
                        >
                            <AccountCircle />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            {renderMenu}
        </>
    );
};


Header.propTypes = {
    cartItems: PropTypes.array
};

export default Header;
