import React, { useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { selectors } from "../../repositories/store/reducer";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { ListItemText, Avatar, ListItemAvatar, Button } from "@material-ui/core";
import { removeQuantity, addToCart, deleteProduct, proceedCheckOut, doCheckOut } from "../../repositories/store/actions";
import CircularProgress from "@material-ui/core/CircularProgress";
import { blue } from "@material-ui/core/colors";
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper
    },
    inline: {
        display: 'inline'
    },
    button: {
        height: "20px",
        minWidth: "20px",
        fontSize: "larger",
        margin: "5px"
    },
    price: {
        textAlign: "center",
        marginBottom: "5px"
    },
    total: {
        float: "right",
        margin: "20px"
    },
    itemControl: {
        marginRight: "15px"
    },
    checkoutContainer: {
        display: "flex",
        alignItems: "center",
        float: "right",
        flexDirection: "column",
        padding: "16px"
    },
    buttonProgress: {
        color: blue[500],
        position: "absolute",
        top: "50%",
        left: "50%",
        marginTop: -12,
        marginLeft: -12
    },
    disable: {
        opacity: "0.4",
        pointerEvents: "none"
    }
}));

const Cart = () => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const [loading, setLoading] = useState(false);

    const cartItems = useSelector(state => selectors.getCartItems(state));
    const totalValue = useSelector(state => selectors.getTotalAmount(state));

    const handleRemoveQuantity = (book) => {
        dispatch(removeQuantity(book));
    };

    const handleAddMore = (book) => {
        dispatch(addToCart(book));
    };

    const handleDeleteProduct = (book) => {
        dispatch(deleteProduct(book));
    };

    const handleProceedCheckOut = () => {
        setLoading(true);

        doCheckOut()
            .then(() => {
                dispatch(proceedCheckOut());
                setLoading(false);
            })
            .catch(error => (console.log(error)));
    };

    return (
        <List className={loading ? classes.disable : classes.root}>
            {cartItems.map((book, index) => (
                <div key={index}>
                    <ListItem alignItems="flex-start">
                        <ListItemAvatar>
                            <Avatar alt="Remy Sharp" src={book.image}/>
                        </ListItemAvatar>
                        <ListItemText primary={book.title}
                            secondary={book.content}/>
                        <div className={classes.itemControl}>
                            <div className={classes.price}>
                                {book.qty} x {book.price} lei = {(book.qty * book.price).toFixed(2)} lei
                            </div>
                            <Button className={classes.button}
                                variant="contained"
                                color="secondary"
                                onClick={() => handleRemoveQuantity(book)}>
                                -
                            </Button>
                            <Button className={classes.button}
                                variant="contained"
                                color="primary"
                                onClick={() => handleAddMore(book)}>
                                +
                            </Button>
                            <Button className={classes.button}
                                onClick={() => handleDeleteProduct(book)}>
                                <DeleteIcon/>
                            </Button>
                        </div>
                    </ListItem>
                    <Divider variant="insert" component="li"/>
                </div>
            ))}
            <div className={classes.checkoutContainer}>
                <div className={classes.total}>Total: {totalValue.toFixed(2)} Lei</div>
                <Button variant="contained"
                    disabled={loading || !cartItems.length}
                    onClick={handleProceedCheckOut}
                    color="primary">
                    Check Out
                </Button>

            </div>
            {loading && (
                <CircularProgress size={35} className={classes.buttonProgress}/>
            )}
        </List>
    );
};

export default Cart;
