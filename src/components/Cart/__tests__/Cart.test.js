import * as reactRedux from 'react-redux';
import { render } from "@testing-library/react";
import Cart from "../Cart";
import { selectors } from "../../../repositories/store/reducer";

const item1 = { id: 1, title: 't1', image: 'img 01', description: 'description', price: 12, qty: 5 };
const item2 = { id: 2, title: 't2', image: 'img 02', description: 'description', price: 9, qty: 4 };
const item3 = { id: 3, title: 't2', image: 'img 03', description: 'description', price: 5, qty: 3 };
const store = {
    appReducer: {
        cartItems: [item1, item2, item3],
        favouriteItems: [item2]
    }
};

describe('render', () => {
    const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');
    const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');

    beforeEach(() => {
        useSelectorMock.mockClear();
        useDispatchMock.mockClear();
    });

    it('render cart', () => {
        useSelectorMock.mockReturnValueOnce(selectors.getCartItems(store));
        useSelectorMock.mockReturnValueOnce(selectors.getTotalAmount(store));

        const rendered = render(<Cart/>);
        expect(rendered).toMatchSnapshot();
    });
});


