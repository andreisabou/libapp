import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector, useDispatch } from 'react-redux';
import { selectors } from "../../repositories/store/reducer";
import { Avatar, Button, ListItemAvatar, ListItemText } from "@material-ui/core";
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from "@material-ui/core/Divider";
import DeleteIcon from "@material-ui/icons/Delete";
import { removeFromFavourite } from "../../repositories/store/actions";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper
    },
    inline: {
        display: 'inline'
    },
    button: {
        height: "20px",
        minWidth: "20px",
        fontSize: "larger",
        margin: "5px"
    },
    price: {
        textAlign: "center",
        marginBottom: "5px"
    },
    total: {
        float: "right",
        margin: "20px"
    },
    itemControl: {
        marginRight: "15px",
        display: "inline-flex",
        width: "150px",
        alignItems: "center",
        flexDirection: "column"
    },
    row: {
        display: "flex",
        alignItems: "center",
        padding: "5px"
    }
}));
const Favourite = () => {
    const classes = useStyles();
    const favouriteItems = useSelector(state => selectors.getFavouriteItems(state));
    const dispatch = useDispatch();

    const handleRemoveItem = (item) => {
        dispatch(removeFromFavourite(item));
    };

    return (
        <List className={classes.root}>
            {favouriteItems.map((item, index) => (
                <>
                    <div className={classes.row} key={index}>
                        <ListItem alignItems="flex-start">
                            <ListItemAvatar>
                                <Avatar alt="Remy Sharp" src={item.image}/>
                            </ListItemAvatar>
                            <ListItemText primary={item.title}/>
                        </ListItem>
                        <div className={classes.itemControl}>
                            <div className={classes.price}>
                                {item.price} lei
                            </div>
                            <Button className={classes.button}
                                onClick={() => handleRemoveItem(item)}>
                                <DeleteIcon/>
                            </Button>
                        </div>
                    </div>
                    <Divider variant="insert" component="li"/>
                </>
            ))}
        </List>
    );
};

export default Favourite;
