import React from 'react';
import { render } from '@testing-library/react';
import Book from '../Book';

it('render book', () => {
    const rendered = render(<Book />);
    expect(rendered).toMatchSnapshot();
});
