import React, { useEffect, useState } from "react";
import Book from "./Book";
import { addToCart, addToFavourite, getBooks } from "../../repositories/store/actions";
import { Grid } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { apiGetBooks } from "../../repositories/store/api";

const Books = () => {
    const [books, setBooks] = useState([]);
    const dispatch = useDispatch();

    const handleAddToCart = (book) => {
        dispatch(addToCart(book));
    };

    const handleAddToFavourite = (book) => {
        dispatch(addToFavourite(book));
    };

    const fetchBooks = () => {
        getBooks().then(response => {
            setBooks(response.data);
        }).catch(e => {
            //IN CASE OF Back-end Application is not started/ not working we use this backup just for demonstration purpose
            //otherwise, ignore this
            apiGetBooks().then(response => {
                setBooks(response);
            });
            console.log('Backend application not working', e);
        });
    };

    useEffect(() => {
        fetchBooks();
    }, []);

    return (
        <Grid container spacing={3} style={{ padding: "20px" }}>
            {books && books.map((book, index) => (
                <Grid item md={3} key={index}>
                    <Book
                        title={book.title}
                        description={book.description}
                        image={book.image}
                        price={book.price}
                        id={book.id}
                        onAddToFavorite={() => handleAddToFavourite(book)}
                        onAddToCart={() => handleAddToCart(book)}
                    />
                </Grid>
            ))}
        </Grid>

    );
};

export default Books;
