import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Button, CardActions, CardContent, CardHeader, CardMedia, IconButton, Typography, Card } from "@material-ui/core";
import FavoriteIcon from '@material-ui/icons/Favorite';
import { yellow } from '@material-ui/core/colors';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 345
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    },
    addToCart: {
        backgroundColor: yellow[500]
    },
    price: {
        fontSize: 'larger'
    },
    cardActions: {
        justifyContent: 'space-evenly'
    }
}));

const Book = ({ title, image, description, price, onAddToCart, onAddToFavorite }) => {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardHeader
                title={title}
                subheader="May 8, 2021"/>
            <CardMedia
                className={classes.media}
                image={image}
                title=""/>
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    {description}
                </Typography>
            </CardContent>
            <CardActions className={classes.cardActions}>
                <IconButton aria-label="add to favorites"
                    onClick={onAddToFavorite}>
                    <FavoriteIcon/>
                </IconButton>
                <Button variant="contained"
                    className={classes.addToCart}
                    color="default"
                    onClick={onAddToCart}>
                    Add to cart
                </Button>
                <Typography className={classes.price} display="block" align="left" color="textSecondary">
                    {price} lei
                </Typography>
            </CardActions>
        </Card>
    );
};


Book.propTypes = {
    title: PropTypes.string,
    image: PropTypes.string,
    description: PropTypes.string,
    price: PropTypes.number,
    onAddToCart: PropTypes.func,
    onAddToFavorite: PropTypes.func,
    id: PropTypes.number
};

export default Book;
