import axios from "axios";
import books from "../../assets/books.json";
import { userRequest, userResponse, VALID_TOKEN } from "../../assets/utils";

const DEFAULT_HEADERS = {
    "content-type": "application/json"
};

const LOCAL_STORAGE_TOKEN = 'token';

/**
 * on each request grab de the auth headers
 * and persist it to a local storage
 */
axios.interceptors.response.use(
    response => {
        const accessToken = response.headers["access-token"];

        if (accessToken) {
            localStorage.setItem(
                LOCAL_STORAGE_TOKEN,
                accessToken
            );
        }
        return response;
    },
    error => Promise.reject(error)
);

/**
 * send auth headers
 */
axios.interceptors.request.use(
    config => {
        const userToken = localStorage.getItem(LOCAL_STORAGE_TOKEN);
        Object.assign(config.headers.common, {
            "access-token": userToken
        });

        return config;
    },
    error => Promise.reject(error)
);

export const apiRequest = async ({ path, method = "GET", data, headers = {} }) => {
    try {
        return await axios({
            method,
            url: path,
            data: data || {},
            headers: DEFAULT_HEADERS
        });
    } catch (error) {
        throw new Error(error);
    }
};

export const apiCheckout = async () => {
    try {
        // eslint-disable-next-line no-magic-numbers
        return new Promise(resolve => setTimeout(resolve, 3000));
    } catch (error) {
        return error;
    }
};


//***************************************************************************************
//BACKUP if the back-end application not working
export const apiGetBooks = async () => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(books);
            // eslint-disable-next-line no-magic-numbers
        });
    });
};

//BACKUP if the back-end application not working
export const apiGetUser = async (data) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (data.email === userRequest.email && data.password === userRequest.password) {
                resolve(userResponse);
            } else {
                reject(new Error("Invalid Credentials"));
            }
            // eslint-disable-next-line no-magic-numbers
        }, 100);
    });
};

//BACKUP if the back-end application not working
export const apiValidateToken = async (token) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (VALID_TOKEN === token) {
                resolve(userResponse);
            } else {
                reject(new Error("Invalid Credentials"));
            }
            // eslint-disable-next-line no-magic-numbers
        }, 100);
    });
};

//**********************************************************************************************
