import { apiCheckout, apiRequest } from "./api";

const Types = {
    ADD_TO_CART: 'ADD_TO_CART',
    REMOVE_QUANTITY: 'REMOVE_QUANTITY',
    DELETE_PRODUCT: 'DELETE_PRODUCT',
    ADD_TO_FAVOURITE: 'ADD_TO_FAVOURITE',
    REMOVE_FAVOURITE: 'REMOVE_FAVOURITE',
    PROCEED_CHECKOUT: 'PROCEED_CHECKOUT'
};

export const addToCart = (item) => ({
    type: Types.ADD_TO_CART,
    payload: item
});

export const addToFavourite = (item) => ({
    type: Types.ADD_TO_FAVOURITE,
    payload: item
});

export const removeFromFavourite = (item) => ({
    type: Types.REMOVE_FAVOURITE,
    payload: item
});

export const removeQuantity = (item) => ({
    type: Types.REMOVE_QUANTITY,
    payload: item
});

export const deleteProduct = (product) => ({
    type: Types.DELETE_PRODUCT,
    payload: product
});

export const proceedCheckOut = () => ({
    type: Types.PROCEED_CHECKOUT
});

export const doCheckOut = async () => {
    return await apiCheckout();
};

export const login = async ({ email, password }) => {
    return await apiRequest({
        path: '/auth/login',
        method: 'POST',
        data: { email, password }
    });
};

export const authenticate = ({ token, secret }) => {
    return apiRequest({
        path: "/auth/validate_token",
        method: "POST",
        data: { token, secret }
    });
};

export const getBooks = async () => {
    return await apiRequest({
        path: "/book",
        method: "GET"
    });
};

export default {
    Types
};
