import { selectors } from "../reducer";

describe('selectors', () => {
    const item1 = { id: 1, title: 't1', image: 'img 01', description: 'description', price: 12, qty: 5 };
    const item2 = { id: 2, title: 't2', image: 'img 02', description: 'description', price: 9, qty: 4 };
    const item3 = { id: 3, title: 't2', image: 'img 03', description: 'description', price: 5, qty: 3 };
    const store = {
        appReducer: {
            cartItems: [item1, item2, item3],
            favouriteItems: [item1, item3]
        }
    };

    it('should return the total amount, reduce from items to one value', () => {
        const expectedAmount = 111;
        expect(selectors.getTotalAmount(store)).toEqual(expectedAmount);
    });

    it('should get all items from cart', () => {
        const expectedValue = [item1, item2, item3];
        expect(selectors.getCartItems(store)).toEqual(expectedValue);
    });

    it('should get all items from favourite', () => {
        const expectedValue = [item1, item3];
        expect(selectors.getFavouriteItems(store)).toEqual(expectedValue);
    });
});
