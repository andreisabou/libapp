import ACTIONS, { addToFavourite, addToCart, removeFromFavourite, removeQuantity, deleteProduct } from '../actions';

describe('actions', () => {
    const item = { title: 'test', image: 'img 01', description: 'description', price: 12 };

    it('should create an action to add item to cart', () => {
        const expectedAction = {
            type: ACTIONS.Types.ADD_TO_CART,
            payload: item
        };

        expect(addToCart(item)).toEqual(expectedAction);
    });

    it('should create an action to add item to favourite', () => {
        const expectedAction = {
            type: ACTIONS.Types.ADD_TO_FAVOURITE,
            payload: item
        };

        expect(addToFavourite(item)).toEqual(expectedAction);
    });

    it('should remove one item from favourite', () => {
        const expectedAction = {
            type: ACTIONS.Types.REMOVE_FAVOURITE,
            payload: item
        };

        expect(removeFromFavourite(item)).toEqual(expectedAction);
    });

    it('should remove one quantity of product', () => {
        const expectedAction = {
            type: ACTIONS.Types.REMOVE_QUANTITY,
            payload: item
        };

        expect(removeQuantity(item)).toEqual(expectedAction);
    });

    it('should delete a product', () => {
        const expectedAction = {
            type: ACTIONS.Types.DELETE_PRODUCT,
            payload: item
        };

        expect(deleteProduct(item)).toEqual(expectedAction);
    });
});
