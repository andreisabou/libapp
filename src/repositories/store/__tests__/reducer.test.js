import { appReducer as reducer } from "../reducer";
import ACTIONS from '../actions';

describe('appReducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(
            {
                cartItems: [],
                favouriteItems: []
            }
        );
    });

    it('should handle ADD_TO_CART first item', () => {
        const item = { title: 'test', image: 'img 01', description: 'description', price: 12 };
        expect(
            reducer(undefined, {
                type: ACTIONS.Types.ADD_TO_CART,
                payload: item
            })
        ).toEqual({
            cartItems: [{ ...item, qty: 1 }],
            favouriteItems: []
        });
    });

    it('should handle ADD_TO_CART increase qty', () => {
        const item = { title: 'test', image: 'img 01', description: 'description', price: 12 };
        const state = {
            cartItems: [{ ...item, qty: 1 }],
            favouriteItems: []
        };
        expect(
            reducer(state, {
                type: ACTIONS.Types.ADD_TO_CART,
                payload: item
            })
        ).toEqual({
            cartItems: [{ ...item, qty: 2 }],
            favouriteItems: []
        });
    });

    it('should handle DELETE_PRODUCT and remove product from cart', () => {
        const item1 = { title: 't1', image: 'img 01', description: 'description', price: 12 };
        const item2 = { title: 't2', image: 'img 02', description: 'description', price: 9 };
        const item3 = { title: 't2', image: 'img 03', description: 'description', price: 5 };
        const state = {
            cartItems: [item1, item2, item3],
            favouriteItems: []
        };
        expect(
            reducer(state, {
                type: ACTIONS.Types.DELETE_PRODUCT,
                payload: item1
            })
        ).toEqual({
            cartItems: [item2, item3],
            favouriteItems: []
        });
    });

    it('should handle REMOVE_QUANTITY and remove the qty from product', () => {
        const item1 = { id: 1, title: 't1', image: 'img 01', description: 'description', price: 12, qty: 5 };
        const item2 = { id: 2, title: 't2', image: 'img 02', description: 'description', price: 9, qty: 4 };
        const item3 = { id: 3, title: 't2', image: 'img 03', description: 'description', price: 5, qty: 3 };
        const state = {
            cartItems: [item1, item2, item3],
            favouriteItems: []
        };
        expect(
            reducer(state, {
                type: ACTIONS.Types.REMOVE_QUANTITY,
                payload: item2
            })
        ).toEqual({
            cartItems: [
                item1,
                {
                    id: 2,
                    title: 't2',
                    image: 'img 02',
                    description: 'description',
                    price: 9,
                    qty: 3
                },
                item3
            ],
            favouriteItems: []
        });
    });

    it('should handle REMOVE_QUANTITY and remove the product from cart if qty = 1', () => {
        const item1 = { id: 1, title: 't1', image: 'img 01', description: 'description', price: 5, qty: 1 };
        const item2 = { id: 2, title: 't2', image: 'img 02', description: 'description', price: 9, qty: 4 };

        const state = {
            cartItems: [item1, item2],
            favouriteItems: []
        };
        expect(
            reducer(state, {
                type: ACTIONS.Types.REMOVE_QUANTITY,
                payload: item1
            })
        ).toEqual({
            cartItems: [item2],
            favouriteItems: []
        });
    });
});
