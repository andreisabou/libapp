import ACTIONS from './actions';
import _ from 'lodash';

const defaultState = {
    cartItems: [],
    favouriteItems: []
};

// actions
const removeQuantity = (state, product) => {
    let newState = _.cloneDeep(state);

    if (product.qty === 1) {
        newState.cartItems = newState.cartItems.filter(item => item.id !== product.id);
    } else {
        newState.cartItems = newState.cartItems.map(item => item.id === product.id ?
            { ...product, qty: product.qty - 1 } : item);
    }

    return newState;
};

const addToCart = (state, product) => {
    let newState = _.cloneDeep(state);
    const exist = newState.cartItems.find(item => item.id === product.id);
    if (exist) {
        newState.cartItems = newState.cartItems.map(item => item.id === exist.id ?
            { ...exist, qty: exist.qty + 1 } : item);
    } else {
        newState.cartItems.push({ ...product, qty: 1 });
    }

    return newState;
};

const addToFavourite = (state, product) => {
    let newState = _.cloneDeep(state);
    const exist = newState.favouriteItems.find(item => item.id === product.id);

    if (!exist) {
        newState.favouriteItems.push(product);
    }

    return newState;
};

const deleteProduct = (state, product) => {
    let newState = _.cloneDeep(state);
    let index = _.findIndex(newState.cartItems, product);
    newState.cartItems.splice(index, 1);

    return newState;
};

const removeFromFavourite = (state, item) => {
    let newState = _.cloneDeep(state);
    let index = _.findIndex(newState.favouriteItems, item);
    newState.favouriteItems.splice(index, 1);

    return newState;
};

const proceedCheckout = (state) => {
    let newState = _.cloneDeep(state);
    newState.cartItems = [];

    return newState;
};

const appReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ACTIONS.Types.ADD_TO_CART: {
            return addToCart(state, action.payload);
        }

        case ACTIONS.Types.REMOVE_QUANTITY: {
            return removeQuantity(state, action.payload);
        }

        case ACTIONS.Types.DELETE_PRODUCT: {
            return deleteProduct(state, action.payload);
        }

        case ACTIONS.Types.ADD_TO_FAVOURITE: {
            return addToFavourite(state, action.payload);
        }

        case ACTIONS.Types.REMOVE_FAVOURITE: {
            return removeFromFavourite(state, action.payload);
        }

        case ACTIONS.Types.PROCEED_CHECKOUT: {
            return proceedCheckout(state);
        }

        default:
            return state;
    }
};

// selectors
const getCartItems = state => {
    return state.appReducer.cartItems;
};

const getTotalAmount = state => {
    return state.appReducer.cartItems.reduce((accumulator, curr) => accumulator + (curr.price * curr.qty), 0);
};

const getFavouriteItems = state => {
    return state.appReducer.favouriteItems;
};

const selectors = {
    getCartItems,
    getTotalAmount,
    getFavouriteItems
};

export { appReducer, selectors };
