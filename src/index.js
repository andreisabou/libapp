import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import configureStore from './repositories/store/store';
import './index.css';
import Auth from "./shared/Auth";
import routes from "./shared/routes";

ReactDOM.render(

    <Provider store={configureStore()}>
        <BrowserRouter>
            <Auth>
                {routes}
            </Auth>
        </BrowserRouter>
    </Provider>
    ,
    document.getElementById('root')
);


