import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";

import Login from "../components/Login/Login";
import Header from "../components/Header/Header";
import Books from "../components/Books/Books";
import Cart from "../components/Cart/Cart";
import Favourite from "../components/Favourites/Favourites";

const routes = currentUser => {
    if (!currentUser) {
        return (
            <Switch>
                <Route path="/login" component={Login}/>
                <Redirect to="/login"/>
            </Switch>
        );
    }

    return (
        <div style={{ overflow: "hidden" }}>
            <Header />
            <Switch>
                <Route path="/books" component={Books}/>
                <Route path="/cart" component={Cart} />
                <Route path="/favourite" component={Favourite} />
                <Redirect to="/books"/>
            </Switch>
        </div>
    );
};

export default routes;
