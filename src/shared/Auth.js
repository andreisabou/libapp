import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { authenticate } from "../repositories/store/actions";
import { apiValidateToken } from "../repositories/store/api";

const defaultContext = {};
export const AuthContext = React.createContext(defaultContext);
const TOKEN = 'token';

const Auth = ({ children }) => {
    const [user, setUser] = useState(null);

    const setCurrentUser = newUser => {
        setUser(newUser);

        if (!newUser) {
            localStorage.removeItem(TOKEN);
        }
    };

    const context = { user, setCurrentUser };

    const validateToken = () => {
        const token = localStorage.token;
        if (token) {
            const data = { token, secret: '' };

            authenticate(data).then(response => {
                setCurrentUser(response);
            }).catch(e => {
                //IN CASE OF Back-end Application is not started/ not working we use this backup just for demonstration purpose
                //otherwise, ignore this
                apiValidateToken(token).then(response => {
                    setCurrentUser(response);
                    localStorage.setItem(
                        'token',
                        response.token
                    );
                }).catch(e => {
                    localStorage.removeItem(TOKEN);
                    console.error(e);
                });
                console.log('BACKEND APPLICATION NOT WORKING, WE ARE USING DUMMY DATA', e);
            });
        }
    };

    useEffect(() => {
        validateToken();
    }, []);
    return (
        <AuthContext.Provider value={context} >
            {children(context.user)}
        </AuthContext.Provider>
    );
};

Auth.propTypes = {
    children: PropTypes.func
};

export default Auth;
